#ifndef SERVER_H
	#define SERVER_H
	//Defines -------------------------------------------------------------
	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	#include <netdb.h>
	#include <netinet/in.h>
	#include <sys/socket.h>
	#include <sys/types.h>
	#include <stdbool.h>
	#define SA struct sockaddr
	
	// Functions -----------------------
	void func(int sockfd);
	int registerUser(int sockfd);
	
	//ENUMS
	typedef enum
	{
		CANT_ACCESS_USER_DATABASE = 1,
	} errorCodes;
	
#endif