#include "server.h"

int main(void)
{
	puts("Server time");
	
	int sockfd, connfd, len; 
	struct sockaddr_in servaddr, cli; 
	
	// socket create and verification. 
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
	{
		fprintf(stderr, "Unable to create socket\n");
		return -1;
		
	}
	
	else
	{
		puts("Socket created successfully");
	}
	
	bzero(&servaddr, sizeof(servaddr));
	
	// assign IP, PORT
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(8080);
	
	// Binding newly created socket to given IP and verification. 
	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0)
	{
		fprintf(stderr, "Unable to bind the socket\n");
		return -2;
	}
	else
	{
		puts("Socket successfully bound.");
	}
	
	// Now server is ready to linsten and verify. 
	if ((listen(sockfd, 5)) != 0)
	{
		fprintf(stderr, "Unable to listen\n");
		return -3;
	}
	else
	{
		puts("Server listening...");
	}
	
	len = sizeof(cli);
	
	// Accept the data packet from client and verify. TODO THREAD THIS OFF. 
	connfd = accept(sockfd, (SA*)&cli, &len);
	if (connfd < 0)
	{
		fprintf(stderr, "Server failed to accept connection\n");
		return -4;
	}
	else 
	{
		puts("Server accepted the client's connection");
	}
	
	// Call on func. 
	func(connfd);
	
	// After chatting close the socket. 
	close(sockfd);
	
}

//Function for chat between client and server. 
void func(int sockfd)
{
	char buff[80];
	int n; 
    
    // Basic welcome menu. 
    char *menu = "WELCOME!\n\t/register : Register a new account.\n\t/login : Login\n>";
	// Send it to the client. 
    write(sockfd, menu, strlen(menu));
    
    
	//Infinite loop for chat TODO: Add threading into this. 
	for (;;)
	{
		bzero(buff, 80);
		
		//read the message from client and copy it in buffer. 
		read(sockfd, buff, sizeof(buff));
		//printf("Strlen of buff: %zu\n", strlen(buff));
        if(strncmp(buff, "/register", 9) == 0)
        {
            
            puts("USER NEEDS TO BE REGISTERED.");
            int returnValue = registerUser(sockfd);
            if (returnValue)
            {
                // Anything other than 0 means something bad happened. 
                write(sockfd, "Unable to register at this time. If this continues \
please contact the administrator.", 100);
            }
            else
            {
                write(sockfd, "Goodbye!", 9);
                break;
            }
        }
        else if (strncmp(buff, "exit", 4) == 0)
        {
            write(sockfd, "Goodbye!", 9);
            break;
        }
        else
        {
            write(sockfd, "Invalid option", 15);
        }

	}
}

// Function to register the user. 
int registerUser(int sockfd)
{
     char buff[64];
    char *prompt = "";
    char *menuUsername = "Please enter a username: ";
    char *menuPassword = "Please enter a password: ";
    char *passwordConfirm = "Please re-type the password you entered";
    char *userNameTaken = "The username provided is already taken. Please choose another";
    char *passwordMismatch = "The passwords you entered didn't match. Please try again: ";
    
    
    
    FILE *logins = fopen("cleartext.txt", "r+");
    if (!logins)
    {
        // Unable to open the username / password file. 
        fprintf(stderr, "Unable to access username database\n");
        // Add this to an error log. 
        FILE *errorLog = fopen("errorLog.txt", "a");
        if (!errorLog)
        {
            // Well fuck. 
            fprintf(stderr, "Unable to access errorLog.txt\n");
            return -1; 
        }
        
        // If we can get to the error log. 
        char *toClose = calloc(1, sizeof(buff));
        strncpy(toClose, "date", 4);
        FILE *command = popen(toClose, "r");
        free(toClose);
        fgets(buff, sizeof(buff), command);
        fclose(command);
        printf("%s\n", buff);
        char errorMessage[] = "Unable to access user database - ";
        strncat(errorMessage, buff, strlen(buff));
        strncat(errorMessage, "\n", 3);
        printf("---> %s\n", errorMessage);
        fwrite(errorMessage, 1, strlen(errorMessage), errorLog);
        fclose(errorLog);
        
        // Send some sort of error message to user by return value. 
        return CANT_ACCESS_USER_DATABASE;
    }
    
    // Woot! Made it past the 'can't access file'. Time to actually check file. 
    // First: check every username in file to make sure that the uesrname provided is unique. 
    // TODO:Break this out? 
    char line[64];
    bzero(&line, 64);
    char userName[128];
    bzero(&userName, sizeof(userName));
    int bad = 1; 
    while (bad)
    {
        printf("-> In while loop\n");
        write(sockfd, menuUsername, strlen(menuUsername));
        bzero(&buff, 64);
        read(sockfd, buff, sizeof(buff));
        printf("-> Username to check: %s\n", buff);
        printf("-> Strlen of buff: %zu\n", strlen(buff));
        while (fgets(line, strlen(line), logins) != NULL)
        {
            puts("--> In second while loop");
            printf("---> '%s'\n", line);
            if(!line)
            {
                puts("???");
            }
            // As long as there's line in it. 
            // Check the username. 
            printf("%s\n", line);
            char *token = strtok(line, " ");
            if (strncmp(token, buff, strlen(buff)) == 0)
            {
                // Username is already taken. 
                write(sockfd, userNameTaken, strlen(userNameTaken));
                bad++;
                break;
            }
        }
        
        if (bad > 1)
        {
            //I don't even fucking know. 
            puts("whut");
            return -1;
        }
        if (bad == 1)
        {
            strncpy(userName, buff, strlen(buff));
        }
        bad--;
    }
    
    bool passwordSet = false;
    write(sockfd, menuPassword, strlen(menuPassword));
    while (!passwordSet)
    {
        // Ok, now we know the username is good. 
        // Get us a password. 
        
        bzero(buff, strlen(buff));
        // Get the password (initial)
        read(sockfd, buff, sizeof(buff));
        printf("First password: %s\n", buff);
        
        // Get same password twice. 
        write(sockfd, passwordConfirm, strlen(passwordConfirm));
        char tempPass[64]; 
        bzero(tempPass, strlen(tempPass));
        read(sockfd, tempPass, sizeof(tempPass));
        fseek(logins, 0, SEEK_END);
        
        //TEsting
        printf("Buf: %s\n", buff);
        printf("tempPass: %s\n", tempPass);
        if (strncmp(buff, tempPass, strlen(buff)) == 0)
        {
            // They're the same, add it to the file. 
            // The cursor should be at the end of the file now. 
            // Let's test that theory. 
            printf("Username: %s\n", userName);
            strncat(userName, " ", 1);
            strncat(userName, tempPass, strlen(tempPass));
            strncat(userName, "\n", 1);
            printf("Adding '%s'\n", userName);
            fwrite(userName, 1, strlen(userName), logins); 
            passwordSet = true; 
        }
        else
        {
            // Passwords didn't match. 
            write(sockfd, passwordMismatch, strlen(passwordMismatch));
            
        }
    }
    fclose(logins);
    return 0;
    
}