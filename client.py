#!/usr/bin/env python3

import socket
import sys
import binascii
import subprocess

HOST = '127.0.0.1' #TODO change this to find the server's host name / ip ? 
PORT = 8080

def main():
    print("# Creating socket.")
    
    # Create the socket. Don't forget about the try-except blocks. 
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    except socket.error:
        print("Failed to create socket.")
        sys.exit()
        
    # Get the IP. I currently already have it. so TODO
    
    
    # Connect to the server using the IP. 
    print("## Connecting to Server")
    try:
        s.connect((HOST, PORT))
    except socket.error:
        print("Failed to connect to", HOST)
        sys.exit()
        
    
    # Wait until menu is recieved from the server. 
    recieved = (s.recv(1024)).decode('ascii')
    while (recieved != "Goodbye!\n"):
        print("'{}'".format(recieved))
        # Send "Request" to server. 
        # Testing just the word: /register. 
        #print("### Sending '/register' to the server")
        if ('password' in recieved):
            # If the user is being prompted for a 
            # password set up the shell so it isn't 
            # dipslayed. I can do this
            hidePassword = "stty -echo"
            unHide = "stty echo"
            process1 = subprocess.Popen(hidePassword.split(), stdout=subprocess.PIPE)
            output, error = process1.communicate()
            
            # Let them Enter their password. 
            userInput = input()
            # Then turn on echo again. 
            process1 = subprocess.Popen(unHide.split(), stdout=subprocess.PIPE)
            output, error = process1.communicate()
        elif (recieved == "Goodbye!"):
            print("Exiting Now...")
            sys.exit()
        else:
            # Get it from the user. 
            userInput = input()
        #print("-->", userInput)
        try:
            s.sendall(binascii.a2b_qp(userInput))
            # It's getting stuck here. Waiting. 
            #recieved = (s.recv(1024)).decode('ascii')
        except socket.error:
            print("Unable to send '{}'".format(userInput))
            sys.exit()
        if (userInput == "exit"):
            sys.exit()
        
            
        recieved = (s.recv(1024)).decode('ascii')
    '''

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        s.sendall(b'exit')
        data = s.recv(1024)
        readable = data.decode("ascii")
        print("-->", readable)
        
    print('Recieved', data)'''

if __name__ == "__main__":
    main()